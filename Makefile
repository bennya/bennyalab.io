emacs ?= emacs
BEMACS = $(emacs) --batch -Q


build:
	$(BEMACS) -l emacs-lisp/build.el
	hugo


.PHONY: build
